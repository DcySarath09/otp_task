package com.sarath.task.ui.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SignupResponse implements Serializable {

    @Expose
    @SerializedName("Status")
    public String Status;
    @Expose
    @SerializedName("country_code")
    public String country_code;
    @Expose
    @SerializedName("otp_code_mobile")
    public String otp_code_mobile;
    @Expose
    @SerializedName("mobile_no")
    public String mobile_no;
}
