package com.sarath.task.ui.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.sarath.task.R;
import com.sarath.task.base.BaseActivity;
import com.sarath.task.common.LoginSession;
import com.sarath.task.databinding.SignupScreenBinding;
import com.sarath.task.service.RequestID;
import com.sarath.task.service.ServerListener;
import com.sarath.task.service.ServerRequest;
import com.sarath.task.ui.otp.OtpVerificationScreen;

import java.util.HashMap;
import java.util.Map;

public class SignupScreen extends BaseActivity implements ServerListener {

    /*Bind View*/
    SignupScreenBinding binding;

    /*Create server objects*/
    ServerRequest serverRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.signup_screen);
        showBackArrow();
        setTitle("SignUp");

        /*Initialize objects*/
        serverRequest = ServerRequest.getInstance(this);

        binding.continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = binding.nameEditText.getText().toString().trim();
                String number = binding.numberEditText.getText().toString().trim();

                if(name.isEmpty()){
                    binding.nameEditText.setError("Please enter a name");
                }else if(number.isEmpty()){
                    binding.numberEditText.setError("Please enter a number");
                }else if(number.length()<10){
                    binding.numberEditText.setError("Please enter a valid number");
                }else if(!isConnectingToInternet()){
                    noInternetAlertDialog();
                }else{
                    showProgressDialog();
                    final Map<String, String> param = new HashMap<String, String>();
                    param.put("mobile",number);
                    serverRequest.createRequest(SignupScreen.this,param,RequestID.REQ_SIGNUP);

                }
            }
        });
    }

    @Override
    public void onSuccess(Object result, RequestID requestID) {
        hideProgressDialog();
        SignupResponse signupResponse = (SignupResponse)result;
        if(signupResponse!=null){
            toast("OTP send successfully");
            LoginSession loginSession = LoginSession.getInstance(this);
            loginSession.saveDatas(signupResponse.mobile_no,signupResponse.otp_code_mobile,binding.nameEditText.getText().toString().trim());
            startActivity(new Intent(SignupScreen.this, OtpVerificationScreen.class));
        }
    }

    @Override
    public void onFailure(String error, RequestID requestID) {
        hideProgressDialog();
    }
}
