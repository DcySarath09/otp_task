package com.sarath.task.ui.otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtpResponse implements Serializable {


    @Expose
    @SerializedName("user_auth")
    public int user_auth;
    @Expose
    @SerializedName("user_id")
    public String user_id;
    @Expose
    @SerializedName("Status")
    public String Status;
}
