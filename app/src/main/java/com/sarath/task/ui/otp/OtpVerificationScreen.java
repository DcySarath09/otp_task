package com.sarath.task.ui.otp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.databinding.DataBindingUtil;

import com.sarath.task.R;
import com.sarath.task.base.BaseActivity;
import com.sarath.task.common.LoginSession;
import com.sarath.task.databinding.OtpScreenBinding;
import com.sarath.task.service.RequestID;
import com.sarath.task.service.ServerListener;
import com.sarath.task.service.ServerRequest;
import com.sarath.task.ui.profile.ProfileScreen;

import java.util.HashMap;
import java.util.Map;

public class OtpVerificationScreen extends BaseActivity implements ServerListener {

    /*Bind View*/
    OtpScreenBinding binding;

    /*Create objects*/
    ServerRequest serverRequest;
    LoginSession loginSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.otp_screen);
        showBackArrow();
        setTitle("OTP Verification");

        /*Initialize objects*/
        serverRequest = ServerRequest.getInstance(this);
        loginSession = LoginSession.getInstance(this);

        binding.codeEditText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if(binding.codeEditText1.getText().toString().trim().length() > 0){
                    binding.codeEditText2.requestFocus();
                }else{
                    binding.codeEditText1.requestFocus();
                }

            }
        });


        binding.codeEditText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if(binding.codeEditText2.getText().toString().trim().length() > 0){
                    binding.codeEditText3.requestFocus();
                }else{
                    binding.codeEditText2.requestFocus();
                }

            }
        });

        binding.codeEditText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if(binding.codeEditText3.getText().toString().trim().length() > 0){
                    binding.codeEditText4.requestFocus();
                }else{
                    binding.codeEditText3.requestFocus();
                }

            }
        });

        binding.codeEditText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if(binding.codeEditText4.getText().toString().trim().length() > 0){

                    String one   = binding.codeEditText1.getText().toString().trim();
                    String two   = binding.codeEditText2.getText().toString().trim();
                    String three = binding.codeEditText3.getText().toString().trim();
                    String four  = binding.codeEditText4.getText().toString().trim();

                    String FINALOTP = one+two+three+four;

                    if(FINALOTP.equals(loginSession.getOtp())){

                        if(isConnectingToInternet()){
                            final Map<String, String> param = new HashMap<String, String>();
                            param.put("otp",loginSession.getOtp());
                            param.put("mobile",loginSession.getMobileNumber());
                            showProgressDialog();
                            serverRequest.createRequest(OtpVerificationScreen.this,param, RequestID.REQ_VERIFY_OTP);
                        }else{
                            noInternetAlertDialog();
                        }

                    }else{

                        toast("Please enter a valid OTP");
                        binding.codeEditText1.setText("");
                        binding.codeEditText2.setText("");
                        binding.codeEditText3.setText("");
                        binding.codeEditText4.setText("");
                        binding.codeEditText1.requestFocus();

                    }

                }else{
                    binding.codeEditText4.requestFocus();
                }

            }
        });
    }

    @Override
    public void onSuccess(Object result, RequestID requestID) {
        hideProgressDialog();
        OtpResponse otpResponse = (OtpResponse)result;
        toast("OTP verified successfully");
        startActivity(new Intent(OtpVerificationScreen.this, ProfileScreen.class)
        .putExtra("USERID",otpResponse.user_id));
        finish();
    }

    @Override
    public void onFailure(String error, RequestID requestID) {
        hideProgressDialog();
    }
}
