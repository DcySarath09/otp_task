package com.sarath.task.ui.profile;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.sarath.task.R;
import com.sarath.task.base.BaseActivity;
import com.sarath.task.common.LoginSession;
import com.sarath.task.databinding.ProfileScreenBinding;
import com.sarath.task.service.RequestID;
import com.sarath.task.service.ServerListener;
import com.sarath.task.service.ServerRequest;

import java.util.HashMap;
import java.util.Map;

public class ProfileScreen extends BaseActivity implements ServerListener {

    /*View Bind*/
    ProfileScreenBinding binding;

    /*Create objects*/
    ServerRequest serverRequest;
    LoginSession loginSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.profile_screen);
        showBackArrow();
        setTitle("Profile");

        /*Initialize objects*/
        loginSession = LoginSession.getInstance(this);
        serverRequest = ServerRequest.getInstance(this);

        if(isConnectingToInternet()){
            final Map<String, String> param = new HashMap<String, String>();
            param.put("user_id",getIntent().getStringExtra("USERID"));
            showProgressDialog();
            serverRequest.createRequest(ProfileScreen.this,param,RequestID.REQ_PROFILE);
        }else{
            noInternetAlertDialog();
        }

    }

    @Override
    public void onSuccess(Object result, RequestID requestID) {
        hideProgressDialog();
        ProfileResponse profileResponse = (ProfileResponse)result;
        if(profileResponse.get_user!=null){
            binding.firstNameEditText.setText(profileResponse.get_user.user_id);
            binding.numberEditText.setText(profileResponse.get_user.mobile_no);
        }

    }

    @Override
    public void onFailure(String error, RequestID requestID) {
        hideProgressDialog();
    }
}
