package com.sarath.task.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sarath.task.R;
import com.sarath.task.base.BaseActivity;
import com.sarath.task.ui.signup.SignupScreen;

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        hideActionBar();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, SignupScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        },2000);
    }
}
