package com.sarath.task.ui.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileResponse implements Serializable {


    @Expose
    @SerializedName("get_user")
    public Get_user get_user;

    public static class Get_user {
        @Expose
        @SerializedName("date_created")
        public String date_created;
        @Expose
        @SerializedName("user_application")
        public String user_application;
        @Expose
        @SerializedName("last_login")
        public String last_login;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("country_code")
        public String country_code;
        @Expose
        @SerializedName("otp_code_mobile")
        public String otp_code_mobile;
        @Expose
        @SerializedName("mobile_no")
        public String mobile_no;
        @Expose
        @SerializedName("user_id")
        public String user_id;
    }
}
