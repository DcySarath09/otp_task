package com.sarath.task.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class LoginSession {

    Activity activity;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static LoginSession loginSession=null;

    //Constructor
    public static LoginSession getInstance(Activity activity)
    {
        if (loginSession==null) {
            loginSession = new LoginSession(activity);
        }
        return loginSession;
    }

    public LoginSession(Activity activity) {
        super();
        this.activity = activity;

        preferences = this.activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        editor = preferences.edit();

    }

    //Save device id
    public void saveDatas (String mobile,String otp,String name) {
        editor.putString("mobile", mobile);
        editor.putString("otp", otp);
        editor.putString("name", name);
        editor.commit();
    }

    //get device id
    public String getMobileNumber() {
        String mobile = preferences.getString("mobile", "");
        return mobile;
    }

    public String getOtp() {
        String otp = preferences.getString("otp", "");
        return otp;
    }

    public String getName() {
        String name = preferences.getString("name", "");
        return name;
    }

}
