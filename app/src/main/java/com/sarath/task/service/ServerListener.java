package com.sarath.task.service;


public interface ServerListener {

    public void onSuccess(Object result, RequestID requestID);

    public void onFailure(String error, RequestID requestID);

}
