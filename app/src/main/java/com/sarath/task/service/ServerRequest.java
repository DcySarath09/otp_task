package com.sarath.task.service;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.sarath.task.ui.otp.OtpResponse;
import com.sarath.task.ui.profile.ProfileResponse;
import com.sarath.task.ui.profile.ProfileScreen;
import com.sarath.task.ui.signup.SignupResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class ServerRequest {

    private static RequestQueue queue = null;
    public static ServerRequest serverRequest = null;
    public static ServerListener serverListener = null;
    Map<String, String> param;
    RequestID requestID;
    Context context;
    Activity activity;

    public ServerRequest(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
        }
        this.context = context;
    }

    public static ServerRequest getInstance(Context context) {
        if (serverRequest == null) {
            serverRequest = new ServerRequest(context);
        }
        return serverRequest;
    }

    public void createRequest(ServerListener serverListener1, Map<String, String> params, RequestID requestid) {
        param = params;
        serverListener = serverListener1;
        requestID = requestid;

        Log.e("PASSING DATA", "" + param);
        StringRequest postRequest = new StringRequest(Request.Method.POST, getURL(),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("RESPONSE", "" + response);

                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                switch (requestID) {

                                    case REQ_SIGNUP:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);
                                        break;

                                    case REQ_VERIFY_OTP:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);
                                        break;

                                    case REQ_PROFILE:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);
                                        break;

                                    default:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);

                                        break;
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                serverListener.onFailure("Please try again!", requestID);
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        serverListener.onFailure("Please try again!", requestID);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return param;
            }

        };
        queue.add(postRequest);
    }

    //Get url based on requestID
    public String getURL() {
        String URL = null;

        switch (requestID) {

            case REQ_SIGNUP:
                URL = Constents.SIGNUP_URL;
                break;

            case REQ_VERIFY_OTP:
                URL = Constents.OTP_VERIFIY;
                break;

            case REQ_PROFILE:
                URL = Constents.PROFILE;
                break;

            default:
                URL = Constents.PROFILE;
                break;

        }

        return URL;
    }


    /**
     * Method to give appropriate class to parse the JSON data
     *
     * @return Class based on request ID ser ver class edited s
     */
    @SuppressWarnings("rawtypes")
    private Class getModel() {
        Class model = null;
        switch (requestID) {

            case REQ_SIGNUP:
                model = SignupResponse.class;
                break;

            case REQ_VERIFY_OTP:
                model = OtpResponse.class;
                break;

            default:
                model = ProfileResponse.class;
                break;
        }

        return model;
    }

    private Object getJsonModelType(String data) {
        Object result = null;
        try {
            Gson gson = new Gson();
            result = gson.fromJson(data, getModel());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
