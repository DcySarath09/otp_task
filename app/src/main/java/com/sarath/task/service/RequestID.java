package com.sarath.task.service;


public enum RequestID {

    //GET STARTING
    REQ_SIGNUP,

    REQ_VERIFY_OTP,

    REQ_PROFILE
}
